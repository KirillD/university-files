#include "stdafx.h"
#include <iostream>
#include <fstream> 
#include <clocale>

using namespace std;

void per(int k);
void comb(int k);

ifstream file("test.txt");
ofstream res1("result1.txt");
ofstream res2("result2.txt");
//ofstream debug("debug.txt");

int counts = 0;
int x = 0;
int *R;
int *wordsIndexes;
int *wordsIndexesComb;
int m = 0;
char text[50][11];

int main()
{
	
	if (file.is_open())
		cout << "File was found" << endl;
	else cout << "Error: can't find such file in the directory of deploying" << endl;

	cout << endl;
	while (!file.eof())
	{
		file >> text[x];   //запись содержимого файла в массив
		x++;
	}

	cout << "Words from file: " << endl;
	for (int i = 0; i < x; i++) cout << text[i] << " ";
	cout << endl << endl;
	file.close();


	wordsIndexes = new int[x];
	wordsIndexesComb = new int[x];

	for (int i = 0; i < x; i++)
	{
		wordsIndexes[i] = i;
	}

	wordsIndexesComb[0] = 0;
	for (int i = 1; i < x; i++)
	{
		wordsIndexesComb[i] = i;
	}

	R = new int[x];     // Массив R проверяет, участвовало ли наше слово в перестановке
	for (int i = 0; i < x; i++) R[i] = 0;
	per(0);


	counts = 0;
	for (m = 1; m <= x; m++)
	{
		comb(1);
	}
	
	/*for (int i = 0; i < x; i++)
	{
		debug << x << ' ';
		debug << text[i] << ' ';
	}
	*/
	return 0;
}


void per(int k)
{
	for (int i = 0; i < x; i++)
	{
		if (R[i] == 0)
		{
			wordsIndexes[k] = i;
			R[i] = 1;
			if (k == x - 1)
			{
				counts++;
				res1 << counts << ") ";
				for (int j = 0; j < x; j++)
					res1 << text[wordsIndexes[j]] << " ";
				res1 << endl;
			}
			else per(k + 1);
			R[i] = 0;
		}
	}
}


void comb(int k)
{
	for (int i = wordsIndexesComb[k - 1] + 1; i <= x - m + k; i++)
	{
		wordsIndexesComb[k] = i;
		if (k == m)
		{
			counts++;
			res2 << counts << ") ";
			for (int j = 1; j <= m; j++)
			{
				res2 << text[wordsIndexesComb[j] - 1] << " ";
			}
			res2 << m << endl;
		}
		else comb(k + 1);
	}
}